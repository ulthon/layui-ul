> 注意：需要引入font awesome 字体文件，但也可以使用任何其他的字体文件或图片。只不过使用图片的话，内置的文件类型的图标颜色就不能用了

内置实现的fa图标如下：

```
fa-file-excel
fa-file-zipper
fa-file-word
fa-file-pdf
fa-file-powerpoint
fa-file-audio
fa-file-code
fa-file-image
fa-file-video
fa-file-text
```

其实可以传入任何图标：

```
fa-file
fa-link
```

以上图标仅供参考，

内置实现的颜色如下：

```
file: #bbb;
excel: rgb(32, 115, 70);
zip: rgb(139, 87, 42);
word: rgb(44, 86, 154);
powerpoint:242, 97, 63);
pdf: rgb(250, 81, 81);
video: #7c8eee;
audio: #f16c00;
image: #f6ad00;
text: #8289ad;
link: #4876f9;
```